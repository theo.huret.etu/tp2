"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Component = /*#__PURE__*/function () {
  function Component(tagName, attribute, children) {
    _classCallCheck(this, Component);

    _defineProperty(this, "tagName", void 0);

    _defineProperty(this, "children", void 0);

    _defineProperty(this, "attribute", void 0);

    this.tagName = tagName;
    this.children = children;
    this.attribute = attribute;
  }

  _createClass(Component, [{
    key: "render",
    value: function render() {
      if (this.children) {
        if (this.attribute) {
          return "<".concat(this.tagName, " ").concat(this.attribute.name, "=\"").concat(this.attribute.value, "\">").concat(this.children, "</").concat(this.tagName, ">");
        }

        return "<".concat(this.tagName, ">").concat(this.children, "</").concat(this.tagName, ">");
      }

      if (this.attribute) {
        return "<".concat(this.tagName, " ").concat(this.attribute.name, "=\"").concat(this.attribute.value, "\"/>");
      }

      return "<".concat(this.tagName, "/>");
    }
  }]);

  return Component;
}();

exports["default"] = Component;
//# sourceMappingURL=Component.js.map