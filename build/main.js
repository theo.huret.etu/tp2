"use strict";

var _data = _interopRequireDefault(require("./data.js"));

var _Component = _interopRequireDefault(require("./Component.js"));

var _Img = _interopRequireDefault(require("./components/Img.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var title = new _Component["default"]('h1', null, 'La carte');
console.log(title.render());
document.querySelector('.pageTitle').innerHTML = title.render();
var img = new _Img["default"]('https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300');
document.querySelector('.pageContent').innerHTML = img.render();
var pizza = _data["default"][0];
var pizzaThumbnail = new PizzaThumbnail(pizza);
document.querySelector('.pageContent').innerHTML = pizzaThumbnail.render();
//# sourceMappingURL=main.js.map