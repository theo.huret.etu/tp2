export default class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}
	renderAttribute() {
		return `${this.attribute.name}="${this.attribute.value}"`;
	}
	render() {
		if (this.children && this.attribute == null)
			return `<${this.tagName}>${this.renderChildren(this.children)}</${
				this.tagName
			}>`;
		else if (this.children && this.attribute)
			return `<${this.tagName} ${this.renderAttribute()}>${this.renderChildren(
				this.children
			)}</${this.tagName}>`;
		else if (this.attribute && this.children == null)
			return `<${this.tagName} ${this.renderAttribute()} />`;
		return `<${this.tagName} />`;
	}
	renderChildren(children) {
		if (children instanceof Array)
			return children.map(child => this.renderChildren(child)).join('');

		if (children instanceof Component) return children.render();

		return children;
	}
}

