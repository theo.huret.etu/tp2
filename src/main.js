import data from './data.js';
import Component from './components/Component.js';
import Img from './components/Img.js';
import PizzaThumbnail from './components/PizzaThumbnail.js';
import PizzaList from './pages/PizzaList.js';


const title = new Component( 'h1', null, "La Carte" );
document.querySelector('.pageTitle').innerHTML = title.render();



const pizza = data[0];
const pizzaThumbnail = new PizzaThumbnail(pizza);
document.querySelector( '.pageContent' ).innerHTML = pizzaThumbnail.render();

const pizzaList = new PizzaList(data);
document.querySelector( '.pageContent' ).innerHTML = pizzaList.render();

/*Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [
	{path: '/', page: pizzaList, title: 'La carte'}
];*/


